package com.tongji.pintu.backend.userservice.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

public class UserInformation {
    @ApiModelProperty(value = "用户id", position = 1)
    private Integer userId;

    @ApiModelProperty(value = "昵称", position = 2)
    private String name;

    @ApiModelProperty(value = "头像路径", position = 3)
    private String profilePhotoUrl;

    @ApiModelProperty(value = "性别，具体数字映射关系前端做处理", position = 4)
    private Integer sexual;

    @ApiModelProperty(value = "学校", position = 5)
    private String university;

    @ApiModelProperty(value = "学院", position = 6)
    private String college;

    @ApiModelProperty(value = "专业", position = 7)
    private String major;

    @ApiModelProperty(value = "个人介绍", position = 8)
    private String introduction;

    @ApiModelProperty(value = "年龄", position = 9)
    private Integer age;

    @ApiModelProperty(value = "手机号", position = 10)
    private String phoneNumber;

    @ApiModelProperty(value = "教育经历", position = 11)
    private String education;

    public UserInformation() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public Integer getSexual() {
        return sexual;
    }

    public void setSexual(Integer sexual) {
        this.sexual = sexual;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}

package com.tongji.pintu.backend.userservice.controller;

import com.tongji.pintu.backend.userservice.domain.Tag;
import com.tongji.pintu.backend.userservice.service.TagService;
import com.tongji.pintu.backend.userservice.utils.Response;
import com.tongji.pintu.backend.userservice.utils.ResponseUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@Api(tags = "标签管理服务")
//@RequestMapping("/v1")
public class TagController {
    @Autowired
    private TagService tagService;

    @ApiOperation("创建标签")
    @RequestMapping(value = "/tags", method = RequestMethod.POST)
    public Response createTag(@RequestBody Tag tag){
        if(tagService.createTag(tag)){
            return ResponseUtils.created(null);
        }
        else {
            return ResponseUtils.error(400, "标签名已存在");
        }

    }

    @ApiOperation("更改标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tag",paramType = "body",
                    value = "确保tagId正确，修改tagId对应的标签为所提供的name和description",
                    required = true, dataType = "Tag"),
    })
    @RequestMapping(value = "/tags", method = RequestMethod.PUT)
    public Response updateTag(@RequestBody Tag tag){
        Integer result = tagService.updateTag(tag);
        switch (result){
            case -1:
                return ResponseUtils.error(404, "无法找到此标签");
            case -2:
                return ResponseUtils.error(400, "标签名已存在");
        }
        return ResponseUtils.success(null);

    }

    @ApiOperation("删除标签 #原则上不应当使用，对于不合理标签应当使用修改功能")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tag",paramType = "body",
                    value = "原则上允许按name删除，后续如果有重复标签名需求会更改",
                    required = true, dataType = "Tag"),
    })
    @RequestMapping(value = "/tags", method = RequestMethod.DELETE)
    public Response deleteTag(@RequestBody Tag tag){
        Integer result = tagService.deleteTag(tag);
        switch (result){
            case -1:
                return ResponseUtils.error(404, "无法找到此标签");
        }
        return ResponseUtils.success(null);

    }

    @ApiOperation("查看所有标签")
    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public Response<List<Tag>> getAllTags(){
        return ResponseUtils.success(tagService.getAllTags());
    }

    @ApiOperation("查询标签")
    @RequestMapping(value = "/tags/{id}", method = RequestMethod.GET)
    public Response findTag(@PathVariable("id") Integer id){
        return ResponseUtils.success(tagService.findTagById(id));
    }

    @ApiOperation("当前登录用户添加标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tag",paramType = "body",value = "可以只提供tagId或tagName其中一项",
                    required = true, dataType = "Tag"),
    })
    @RequestMapping(value = "/user/tags", method = RequestMethod.POST)
    public Response userAddTag(HttpServletRequest request, @RequestBody Tag tag){
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("id");
        if(userId == null){
            return ResponseUtils.error(401, "用户未登录");
        }
        Integer result = tagService.userAddTag(userId, tag);
        switch (result){
            case 1:
                return ResponseUtils.success(null);
            case -1:
                return ResponseUtils.error(404, "无法找到此标签");
            case -2:
                return ResponseUtils.error(400, "用户已拥有此标签");
        }
        return ResponseUtils.error(500, "未知错误");
    }

    @ApiOperation("当前登录用户删除标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tag",paramType = "body",value = "可以只提供tagId或tagName其中一项",
                    required = true, dataType = "Tag"),
    })
    @RequestMapping(value = "/user/tags", method = RequestMethod.DELETE)
    public Response userDeleteTag(HttpServletRequest request, @RequestBody Tag tag){
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("id");
        if(userId == null){
            return ResponseUtils.error(401, "用户未登录");
        }
        if(!tagService.userDeleteTag(userId, tag)){
            return ResponseUtils.error(404, "无法找到此标签");
        }
        return ResponseUtils.success(null);

    }

    @ApiOperation("获取指定id用户的所有标签")
    @RequestMapping(value = "/user/{id}/tags", method = RequestMethod.GET)
    public Response<List<Tag>> getUserTags(@PathVariable("id") Integer userId){
        return ResponseUtils.success(tagService.getUserTags(userId));
    }

    @ApiOperation("获取当前登录用户的所有标签")
    @RequestMapping(value = "/user/my/tags", method = RequestMethod.GET)
    public Response<List<Tag>> getMyTags(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("id");
        if(userId == null){
            return ResponseUtils.error(401, "用户未登录");
        }

        return ResponseUtils.success(tagService.getUserTags(userId));
    }

}

package com.tongji.pintu.backend.userservice.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

public class UserFollows {
    @ApiModelProperty(value = "用户id", position = 1)
    private Integer userId;

    @ApiModelProperty(value = "正在关注数", position = 2)
    private Integer followings;

    @ApiModelProperty(value = "粉丝数", position = 3)
    private Integer followers;

    public UserFollows() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFollowings() {
        return followings;
    }

    public void setFollowings(Integer followings) {
        this.followings = followings;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }
}

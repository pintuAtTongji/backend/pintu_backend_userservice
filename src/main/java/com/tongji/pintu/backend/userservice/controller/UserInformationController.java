package com.tongji.pintu.backend.userservice.controller;

import com.tongji.pintu.backend.userservice.domain.UserInformation;
import com.tongji.pintu.backend.userservice.service.UserInformationService;
import com.tongji.pintu.backend.userservice.utils.Response;
import com.tongji.pintu.backend.userservice.utils.ResponseUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = "/user")
@Api(tags = "用户信息服务")
public class UserInformationController {
    @Autowired
    private UserInformationService informationService;

    @ApiOperation("获取指定id的用户信息")
    @RequestMapping(value = "/{id}/info", method = RequestMethod.GET)
    public Response<UserInformation> getInformation(@PathVariable("id") Integer id){
        return ResponseUtils.success(informationService.getInformation(id));
    }


    @ApiOperation(value = "获取当前登录用户的用户信息")
    @RequestMapping(value = "/my/info", method = RequestMethod.GET)
    public Response<UserInformation> myInformation(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        if(id == null){
            return ResponseUtils.error(401,"用户未登录");
        }
        return ResponseUtils.success(informationService.getInformation(id));
    }

    @ApiOperation("更改当前登录用户的用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information",paramType = "body",value = "除了id之外每一项都需要提供，否则置为null",
                    required = true, dataType = "UserInformation"),
    })
    @RequestMapping(value = "/my/info", method = RequestMethod.PUT)
    public Response updateInformation(@RequestBody UserInformation information, HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        if(id == null){
            return ResponseUtils.error(401,"用户未登录");
        }
        information.setUserId(id);
        return ResponseUtils.success(informationService.updateInformation(information));
    }


}

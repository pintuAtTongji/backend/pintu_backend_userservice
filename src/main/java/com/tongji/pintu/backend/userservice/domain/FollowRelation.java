package com.tongji.pintu.backend.userservice.domain;

import io.swagger.annotations.ApiModelProperty;

public class FollowRelation {
    @ApiModelProperty(value = "主id", position = 1)
    private Integer userId;

    @ApiModelProperty(value = "从id", position = 2)
    private Integer targetId;

    @ApiModelProperty(value = "主id是否关注从id， target is user's following", position = 3)
    private Integer isFollowing;

    @ApiModelProperty(value = "从id是否关注主id， target is user's follower", position = 4)
    private Integer isFollower;

    public FollowRelation(Integer userId, Integer targetId, Integer isFollowing, Integer isFollower) {
        this.userId = userId;
        this.targetId = targetId;
        this.isFollowing = isFollowing;
        this.isFollower = isFollower;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public Integer getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(Integer isFollowing) {
        this.isFollowing = isFollowing;
    }

    public Integer getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(Integer isFollower) {
        this.isFollower = isFollower;
    }
}

package com.tongji.pintu.backend.userservice.service;

import com.tongji.pintu.backend.userservice.domain.Tag;
import com.tongji.pintu.backend.userservice.mapper.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("tagService")
public class TagService {
    @Autowired
    private TagMapper tagMapper;

    public boolean createTag(Tag tag){
        if(tagMapper.findTagByName(tag.getName()) != null){
            return false;
        }
        tagMapper.createTag(tag);
        return true;
    }

    public Integer updateTag(Tag tag){
        if(tagMapper.findTagById(tag.getId()) == null){
            return -1;
        }
        if(tagMapper.findTagByName(tag.getName()) != null){
            return -2;
        }
        return tagMapper.updateTag(tag);
    }

    public Integer deleteTag(Tag tag){
        if(tagMapper.findTagById(tag.getId()) == null){
            return -1;
        }
        return tagMapper.deleteTag(tag);
    }

    public List<Tag> getAllTags(){
        return tagMapper.getAllTags();
    }

    public List<Tag> getUserTags(Integer userId){
        return tagMapper.getUserTags(userId);
    }

    public Tag findTagById(Integer id){
        return tagMapper.findTagById(id);
    }

    public Tag findTagByName(String name){
        return tagMapper.findTagByName(name);
    }

    public Integer userAddTag(Integer userId, Tag tag){
        if(tag.getId() == null){
            tag = tagMapper.findTagByName(tag.getName());
        } else if (tag.getName() == null){
            tag = tagMapper.findTagById(tag.getId());
        }
        if(tag == null){
            return -1;
        }
        if(tagMapper.userHasTag(userId, tag.getId()) != null){
            return -2;
        }
        tagMapper.userAddTag(userId, tag);
        return 1;
    }

    public boolean userDeleteTag(Integer userId, Tag tag){
        if(tag.getId() == null){
            tag = tagMapper.findTagByName(tag.getName());
        }
        if(tag == null){
            return false;
        }
        tagMapper.userDeleteTag(userId, tag.getId());
        return true;
    }
}

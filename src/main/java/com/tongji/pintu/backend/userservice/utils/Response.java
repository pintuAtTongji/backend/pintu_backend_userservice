package com.tongji.pintu.backend.userservice.utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.relational.core.sql.In;

@ApiModel
public class Response<T> {
    @ApiModelProperty(position = 1)
    private Integer code;

    @ApiModelProperty(position = 2)
    private String message;

    @ApiModelProperty(position = 3)
    private T data;

    public <E> Response() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

package com.tongji.pintu.backend.userservice.controller;

import com.tongji.pintu.backend.userservice.domain.FollowRelation;
import com.tongji.pintu.backend.userservice.domain.UserFollows;
import com.tongji.pintu.backend.userservice.domain.UserInformation;
import com.tongji.pintu.backend.userservice.service.UserFollowsService;
import com.tongji.pintu.backend.userservice.utils.Response;
import com.tongji.pintu.backend.userservice.utils.ResponseUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = "/user/follow")
@Api(tags = "用户关注服务")
public class UserFollowsController {
    @Autowired
    private UserFollowsService userFollowsService;

    @ApiOperation("获取指定id的关注数&粉丝数")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Response<UserFollows> getFollows(@PathVariable("id") Integer id){
        return ResponseUtils.success(userFollowsService.getFollows(id));
    }

    @ApiOperation("获取当前登录用户的关注数&粉丝数")
    @RequestMapping(value = "/my", method = RequestMethod.GET)
    public Response<UserFollows> myFollows(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        if(id == null){
            return ResponseUtils.error(401,"用户未登录");
        }
        return ResponseUtils.success(userFollowsService.getFollows(id));
    }

    @ApiOperation("使用当前登录用户关注指定id")
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Response follow(@PathVariable("id") Integer id, HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("id");
        if(userFollowsService.isFollowing(userId, id)){
            return ResponseUtils.error(400, "已经处于关注状态");
        }
        userFollowsService.follow(userId, id);
        return ResponseUtils.created(null);
    }

    @ApiOperation("使用当前登录用户取消关注指定id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Response unfollow(@PathVariable("id") Integer id, HttpServletRequest request){
        HttpSession session = request.getSession();
        userFollowsService.unfollow((Integer) session.getAttribute("id"), id);
        return ResponseUtils.success(null);
    }

    @ApiOperation("获取指定id的正在关注id列表")
    @RequestMapping(value = "/{id}/followings", method = RequestMethod.GET)
    public Response<List<UserInformation>> getFollowings(@PathVariable("id") Integer id){
        return ResponseUtils.success(userFollowsService.getFollowings(id));
    }

    @ApiOperation("获取指定id的粉丝id列表")
    @RequestMapping(value = "/{id}/followers", method = RequestMethod.GET)
    public Response<List<UserInformation>> getFollowers(@PathVariable("id") Integer id){
        return ResponseUtils.success(userFollowsService.getFollowers(id));
    }

    @ApiOperation("获取当前登录用户的正在关注id列表")
    @RequestMapping(value = "/my/followings", method = RequestMethod.GET)
    public Response<List<UserInformation>> myFollowings(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        if(id == null){
            return ResponseUtils.error(401,"用户未登录");
        }
        return ResponseUtils.success(userFollowsService.getFollowings(id));
    }

    @ApiOperation("获取当前登录用户的粉丝id列表")
    @RequestMapping(value = "/my/followers", method = RequestMethod.GET)
    public Response<List<UserInformation>> myFollowers(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute("id");
        if(id == null){
            return ResponseUtils.error(401,"用户未登录");
        }
        return ResponseUtils.success(userFollowsService.getFollowers(id));
    }

    @ApiOperation("获取userId是否关注targetId以及是否被关注")
    @RequestMapping(value = "/relation", method = RequestMethod.GET)
    public Response<FollowRelation> getRelation(@RequestParam("userId") Integer userId,
                                                @RequestParam("targetId") Integer targetId){
        return ResponseUtils.success(userFollowsService.getRelation(userId, targetId));
    }

}

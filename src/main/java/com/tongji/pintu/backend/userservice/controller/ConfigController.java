package com.tongji.pintu.backend.userservice.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@ApiIgnore
public class ConfigController {

    @Value("${server.port}")
    private Integer port;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String getConfig(HttpServletRequest request){
        String s = "";
        Cookie[] cookies = request.getCookies();
        //HttpSession session = request.getSession();
        if(cookies == null){
            s += "没有cookie，";
        }
        s += "端口号：";
        s += port;
        return s;
    }
}

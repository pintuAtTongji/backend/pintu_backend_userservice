package com.tongji.pintu.backend.userservice.domain;

import io.swagger.annotations.ApiModelProperty;

public class WxBody {
    @ApiModelProperty(value = "用户id", position = 1)
    private Integer id;

    @ApiModelProperty(value = "微信openid", position = 2)
    private String wxId;

    @ApiModelProperty(value = "SessionKey", position = 3)
    private String sessionKey;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }
}

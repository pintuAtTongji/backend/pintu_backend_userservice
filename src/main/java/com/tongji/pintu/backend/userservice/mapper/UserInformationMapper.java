package com.tongji.pintu.backend.userservice.mapper;

import com.tongji.pintu.backend.userservice.domain.UserFollows;
import com.tongji.pintu.backend.userservice.domain.UserInformation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface UserInformationMapper {
    public UserInformation getInformation(Integer userId);

    public Integer updateInformation(UserInformation information);

}

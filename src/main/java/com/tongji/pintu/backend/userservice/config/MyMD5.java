package com.tongji.pintu.backend.userservice.config;

import org.apache.shiro.crypto.hash.Md5Hash;

public class MyMD5 {
    private static String salt = "8d6a56543232b5d86c56323f";
    private static int hashIteration = 2;
    public static String encrypt(String source){
        Md5Hash md5Hash = new Md5Hash(source, salt, hashIteration);
        return md5Hash.toString();
    }
}

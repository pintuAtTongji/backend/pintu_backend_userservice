package com.tongji.pintu.backend.userservice.mapper;

import com.tongji.pintu.backend.userservice.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface UserMapper {
    int register(User user);

    User login(User user);

    Integer checkName(String userName);

    public User wxFind(String wxId);

    public Integer wxCreate(String wxId);


}

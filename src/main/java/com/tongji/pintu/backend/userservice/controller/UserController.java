package com.tongji.pintu.backend.userservice.controller;

import com.tongji.pintu.backend.userservice.config.MyMD5;
import com.tongji.pintu.backend.userservice.config.WxMappingJackson2HttpMessageConverter;
import com.tongji.pintu.backend.userservice.domain.User;
import com.tongji.pintu.backend.userservice.domain.WxBody;
import com.tongji.pintu.backend.userservice.domain.WxJson;
import com.tongji.pintu.backend.userservice.service.UserService;
import com.tongji.pintu.backend.userservice.utils.Response;
import com.tongji.pintu.backend.userservice.utils.ResponseUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(tags = "用户账号服务")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation("注册")
    @RequestMapping(value="/register", method= RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user",paramType = "body",value = "只需要提供用户名&密码&手机号",required = true, dataType = "User"),
    })
    public Response register(@RequestBody User user) {
        if(userService.checkName(user.getUserName()) != null){
            return ResponseUtils.error(400, "用户名已存在");
        }
        user.setPassword(MyMD5.encrypt(user.getPassword()));
        return ResponseUtils.created(this.userService.register(user));
    }

    @ApiOperation("登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user",paramType = "body",value = "只需要提供用户名&密码",required = true, dataType = "User"),
    })
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public Response login(@RequestBody User user, HttpServletRequest request){
        user.setPassword(MyMD5.encrypt(user.getPassword()));
        User resultUser = this.userService.login(user);
        if(resultUser == null){
            return ResponseUtils.error(400, "用户名不存在或密码错误");
        }
        Integer uid = resultUser.getId();
        resultUser.setUserName(user.getUserName());
        HttpSession session = request.getSession();
        session.setAttribute("id",uid);
        //return uid;
        return ResponseUtils.success(resultUser);
    }

    @ApiOperation("微信登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code",value = "wxlogin提供的code",required = true)})
    @RequestMapping(value = "/wx", method = RequestMethod.GET)
    public Response<WxBody> wxLogin(HttpServletRequest request, @RequestParam String code){
        System.out.println(code);
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" +
                "wx1c133c39f443da81" +
                "&secret=" +
                "0c5f354421c981f2eee3312c53cf2d4c" +
                "&js_code=" + code;
        WxJson wxJson = new WxJson();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new WxMappingJackson2HttpMessageConverter());
        wxJson = restTemplate.getForObject(url, WxJson.class);
        if(wxJson == null){
            return ResponseUtils.error(400, "code无效，未能获取用户");
        }
        String openid = wxJson.getOpenid();
        String sessionKey = wxJson.getSession_key();
        if(openid == null || sessionKey == null){
            return ResponseUtils.error(400, "code无效，未能获取用户");
        }
        User user = userService.wxFind(openid);
        if(user == null){
            return ResponseUtils.error(400, "code无效，未能获取用户");
        }
        HttpSession session = request.getSession();
        session.setAttribute("id", user.getId());
        WxBody wxBody = new WxBody();
        wxBody.setId(user.getId());
        wxBody.setWxId(openid);
        wxBody.setSessionKey(sessionKey);
        //System.out.println(text);
        //System.out.println(wxjs.getOpenid());

        return ResponseUtils.success(wxBody);
    }

    @ApiOperation("登出")
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public Response logout(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.removeAttribute("id");
        return ResponseUtils.success(null);
    }
}

package com.tongji.pintu.backend.userservice.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

public class User {
    @ApiModelProperty(value = "用户id", position = 1)
    private Integer id;

    @ApiModelProperty(value = "用户名", position = 2)
    private String userName;

    @ApiModelProperty(value = "密码", position = 3)
    private String password;

    @ApiModelProperty(value = "微信id", position = 4)
    private String wxId;

    @ApiModelProperty(value = "手机号", position = 5)
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User(Integer id, String userName, String password, String wxId) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.wxId = wxId;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }
}

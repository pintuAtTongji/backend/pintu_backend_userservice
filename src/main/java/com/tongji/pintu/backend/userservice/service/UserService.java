package com.tongji.pintu.backend.userservice.service;

import com.tongji.pintu.backend.userservice.domain.User;
import com.tongji.pintu.backend.userservice.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public int register(User user){
        return userMapper.register(user);
    }

    public User login(User user){
        return userMapper.login(user);
    }

    public Integer checkName(String userName){
        return userMapper.checkName(userName);
    }

    public User wxFind(String wxId){
        User user = userMapper.wxFind(wxId);
        if(user == null){
            userMapper.wxCreate(wxId);
            return userMapper.wxFind(wxId);
        }
        return user;
    }

}

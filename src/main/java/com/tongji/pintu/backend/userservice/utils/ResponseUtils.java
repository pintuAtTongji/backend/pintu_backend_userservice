package com.tongji.pintu.backend.userservice.utils;

public class ResponseUtils {
    public static <E> Response<E> success(E element){
        Response<E> response = new Response<>();
        response.setCode(200);
        response.setMessage("Success");
        response.setData(element);
        return response;
    }

    public static <E> Response<E> created(E element){
        Response<E> response = new Response<>();
        response.setCode(201);
        response.setMessage("Created");
        response.setData(element);
        return response;
    }

    public static <E> Response<E> error(Integer code, String message){
        Response<E> response = new Response<>();
        response.setCode(code);
        response.setMessage(message);
        response.setData(null);
        return response;
    }
}

package com.tongji.pintu.backend.userservice.mapper;

import com.tongji.pintu.backend.userservice.domain.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface TagMapper {
    void createTag(Tag tag);

    Integer updateTag(Tag tag);

    Integer deleteTag(Tag tag);

    List<Tag> getAllTags();

    void userAddTag(Integer userId, Tag tag);

    void userDeleteTag(Integer userId, Integer tagId);

    Integer userHasTag(Integer userId, Integer tagId);

    List<Tag> getUserTags(Integer userId);

    Tag findTagById(Integer id);

    Tag findTagByName(String name);
}

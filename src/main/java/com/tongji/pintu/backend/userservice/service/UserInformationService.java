package com.tongji.pintu.backend.userservice.service;

import com.tongji.pintu.backend.userservice.domain.UserFollows;
import com.tongji.pintu.backend.userservice.domain.UserInformation;
import com.tongji.pintu.backend.userservice.mapper.UserInformationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userInformationService")
public class UserInformationService {
    @Autowired
    private UserInformationMapper userInformationMapper;

    public UserInformation getInformation(Integer userId){
        return userInformationMapper.getInformation(userId);
    }

    public Integer updateInformation(UserInformation information){
        return userInformationMapper.updateInformation(information);
    }


}

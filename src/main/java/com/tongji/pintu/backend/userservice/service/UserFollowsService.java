package com.tongji.pintu.backend.userservice.service;

import com.tongji.pintu.backend.userservice.domain.FollowRelation;
import com.tongji.pintu.backend.userservice.domain.UserFollows;
import com.tongji.pintu.backend.userservice.domain.UserInformation;
import com.tongji.pintu.backend.userservice.mapper.UserFollowsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userFollowsService")
public class UserFollowsService {

    @Autowired
    private UserFollowsMapper userFollowsMapper;

    public UserFollows getFollows(Integer userId){
        return userFollowsMapper.getFollows(userId);
    }

    public void follow(Integer userId, Integer targetId){
        userFollowsMapper.follow(userId, targetId);
    }

    public void unfollow(Integer userId, Integer targetId){
        userFollowsMapper.unfollow(userId, targetId);
    }

    public List<UserInformation> getFollowings(Integer userId){
        return userFollowsMapper.getFollowings(userId);
    }

    public List<UserInformation> getFollowers(Integer userId){
        return userFollowsMapper.getFollowers(userId);
    }

    public boolean isFollowing(Integer userId, Integer targetId){
        return userFollowsMapper.isFollowing(userId, targetId) != null;
    }

    public FollowRelation getRelation(Integer userId, Integer targetId){
        FollowRelation relation = new FollowRelation(userId, targetId, 0, 0);
        if (userFollowsMapper.isFollowing(userId, targetId) != null){
            relation.setIsFollowing(1);
        }
        if(userFollowsMapper.isFollower(userId, targetId) != null){
            relation.setIsFollower(1);
        }
        return relation;

    }

}

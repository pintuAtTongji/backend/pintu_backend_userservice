package com.tongji.pintu.backend.userservice;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableSwagger2Doc
public class PintuBackendUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(PintuBackendUserApplication.class, args);
    }

}

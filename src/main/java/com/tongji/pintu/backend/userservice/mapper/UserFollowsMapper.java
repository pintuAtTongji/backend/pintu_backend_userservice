package com.tongji.pintu.backend.userservice.mapper;

import com.tongji.pintu.backend.userservice.domain.UserFollows;
import com.tongji.pintu.backend.userservice.domain.UserInformation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface UserFollowsMapper {

    public UserFollows getFollows(Integer userId);

    public void follow(Integer userId, Integer targetId);

    public void unfollow(Integer userId, Integer targetId);

    public List<UserInformation> getFollowings(Integer userId);

    public List<UserInformation> getFollowers(Integer followedId);

    public Integer isFollowing(Integer userId, Integer targetId);

    public Integer isFollower(Integer userId, Integer targetId);



}
